% function [] = plotBodies(body,param,paramDef,q,qDef)
%  This function plots a 3D view of the kinematic tree.
%  The coordinate system of a body is visualized by a coordinate 
%  frame with green x-axis, red y-axis and blue z-axis.
%  The center of gravity is shown as a magenta circle.
%  The world frame at (0,0,0) is shown as a dashed frame.
%
%  input:
%       body        kinematic tree
%       param       vector of symbolic parameters
%       paramDef    vector of parameter values
%       q           vector of symbolic minimal coordinates
%       qDef        vector of minimal coordinate values
% -> visualization of the (global) kinematic tree.
%
% !! This is part of the proNEu tool for symbolic EoM derivation
% 
% proNEu: tool for symbolic EoM derivation
% Copyright (C) 2011  Marco Hutter, Christian Gehring
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
function [] = plotBodies(body,param,paramDef,q,qDef)

figure('name','body visualization')
hold on
grid on
axis equal
xlabel('x')
ylabel('y')
zlabel('z')
view(3)
l = [];
for i=1:length(body)
    % make line between CoG and previous joint
    myplot3(body(i).kin.I_r_COG,body(i).kin.I_r_O,param,paramDef,q,qDef);
    % make line between joint an predecessor joint
    if body(i).tree.parent ~=0
        li = myplot3(body(body(i).tree.parent).kin.I_r_O,body(i).kin.I_r_O,param,paramDef,q,qDef);
        l = [l,li];
    end
     % draw center of gravity
    plotCoG(body(i).kin.I_r_COG,param,paramDef,q,qDef);
end

% plot CS scaled by the mean length of the links
for i=1:length(body)
    scale = mean(l)/3;
    % plot cs
    plotCS(body(i),param,paramDef,q,qDef,scale)
end

% plot world coord system
% world.kin.A_IB = eye(3);
% world.kin.I_r_COG = [0;0;0];
% world.kin.I_r_O = [0;0;0];
% plotCS(world,param,paramDef,q,qDef,scale)
plotWorldCS(scale)

end

function li = myplot3(r1,r2,param,paramDef,q,qDef)
r1 = subsP(r1,{param,q},{paramDef,qDef});
r2 = subsP(r2,{param,q},{paramDef,qDef});
plot3([r1(1),r2(1)],[r1(2),r2(2)],[r1(3),r2(3)],'k')
li = norm(r1-r2);
end

function [] = plotCoG(rCoG,param,paramDef,q,qDef)
rCoG = subsP(rCoG,{param,q},{paramDef,qDef});
plot3(rCoG(1),rCoG(2),rCoG(3),'mo')
end

function [] = plotCS(body_i,param,paramDef,q,qDef,scale)
A_IB = body_i.kin.A_IB;
A_IB = subsP(A_IB,{param,q},{paramDef,qDef});

rCoG = body_i.kin.I_r_COG;
rCoG = subsP(rCoG,{param,q},{paramDef,qDef});

rO = body_i.kin.I_r_O;
rO = subsP(rO,{param,q},{paramDef,qDef});

I{1} = A_IB*[1;0;0]*scale;
I{2} = A_IB*[0;1;0]*scale;
I{3} = A_IB*[0;0;1]*scale;


col = {'g','r','b'};
for i=1:3
    plot3([rO(1) rO(1)+I{i}(1)],[rO(2) rO(2)+I{i}(2)],[rO(3) rO(3)+I{i}(3)],'Color',col{i})
    plot3([rO(1)+I{i}(1)],[rO(2)+I{i}(2)],[rO(3)+I{i}(3)],'Color',col{i},'Marker','>')
end
end


function [] = plotWorldCS(scale)
A_IB = eye(3);


rO = [0;0;0];

I{1} = A_IB*[1;0;0]*scale;
I{2} = A_IB*[0;1;0]*scale;
I{3} = A_IB*[0;0;1]*scale;

col = {'g','r','b'};
for i=1:3
    plot3([rO(1) rO(1)+I{i}(1)],[rO(2) rO(2)+I{i}(2)],[rO(3) rO(3)+I{i}(3)], ...
            'Color',col{i}, ...
            'LineStyle','--', ...
            'LineWidth',2);
            
    plot3([rO(1)+I{i}(1)],[rO(2)+I{i}(2)],[rO(3)+I{i}(3)],'Color',col{i},'Marker','>')
end
end

function x = subsP(x_sym,oldCellArr,newCellArr)

old = [];
new = [];
for i=1:length(oldCellArr)
    old = [old,reshape(oldCellArr{i},1,length(oldCellArr{i}))];
    new = [new,reshape(newCellArr{i},1,length(newCellArr{i}))];
end

x= subs(x_sym,old,new);
end

