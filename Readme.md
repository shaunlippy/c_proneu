proNEu
----------------------------------------------------------------------------------

A simple Matlab Tool for Analytical Derivation of global Kinematics and Dynamics based on projected Newton-Euler methods.

The tool proNEu uses the MATLAB Symbolic Math Toolbox to derive the analytical global kinematics and equations of motion based on projected Newton-Euler methods. 
Several examples highlight how this tool has to be used. The user chooses the generalized coordinates, actuator and link parameters before setting up a very simple kinematic tree of the entire system. Symbolically, global kinematics and the equations of motion are derived and the user can visually check the robot configuration. In the examples it is outlined, how the user can manually get function files, compiled mex-function, or c-code that can be used or embedded in any simulation environment. 


----------------------------------------------------------------------------------
proNEu: tool for symbolic EoM derivation
Copyright (C) 2011  Marco Hutter, Christian Gehring
 
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
