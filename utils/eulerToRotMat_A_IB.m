% A_IB = eulerToRotMat_A_IB(al,be,ga)
%
% Converts an al-be-ga Euler angle notation to a rotation matrix. 
% This is the inverse transformation 
% (from local/body to global/inertial coordinates),
% computed as 
%
% A_IB = Ax'*Ay'*Az'
%
% where
%
% Ax = [1 0        0;
%       0 cos(al)  sin(al);
%       0 -sin(al) cos(al)];
% 
% Ay = [cos(be) 0 -sin(be);
%       0       1 0;
%       sin(be) 0 cos(be)];
%  
% Az = [cos(ga)  sin(ga) 0;
%       -sin(ga) cos(ga) 0;
%       0        0       1];
%
%
% Remarks:
% - Notation is consistent with script "Dynamik von Mehrkoerpersystemen" 
%   of Prof. C.Glocker, ETH Zurich (2009)
% - Formulation is consistent with SL function 
%   eulerToRotMatInv(Vector a, Matrix R)
%
% proNEu: tool for symbolic EoM derivation
% Copyright (C) 2011  Marco Hutter, Christian Gehring
% 
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% any later version.
% 
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
% 
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
function A_IB = eulerToRotMat_A_IB(al,be,ga)
A_IB = sym(zeros(3));
A_IB(1,1) =  cos(be)*cos(ga);
A_IB(2,1) =  cos(ga)*sin(al)*sin(be) + cos(al)*sin(ga);
A_IB(3,1) = -(cos(al)*cos(ga)*sin(be)) +sin(al)*sin(ga);
A_IB(1,2) = -(cos(be)*sin(ga));
A_IB(2,2) =  cos(al)*cos(ga) - sin(al)*sin(be)*sin(ga);
A_IB(3,2) =  cos(ga)*sin(al) + cos(al)*sin(be)*sin(ga);
A_IB(1,3) =  sin(be);
A_IB(2,3) = -(cos(be)*sin(al));
A_IB(3,3) =  cos(al)*cos(be);